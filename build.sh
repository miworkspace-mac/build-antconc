#!/bin/bash -ex

# CONFIG
prefix="AntConc"
suffix=""
munki_package_name="AntConc"
display_name="AntConc"
icon_name=""
url=`./finder.sh`
url_arm=`./finder-arm.sh`
description="A freeware corpus analysis toolkit for concordancing and text analysis."

# download it (-L: follow redirects)
curl -L -o app.dmg -A 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/536.26.17 (KHTML, like Gecko) Version/6.0.2 Safari/536.26.17' "${url}"
curl -L -o app_arm.dmg -A 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/536.26.17 (KHTML, like Gecko) Version/6.0.2 Safari/536.26.17' "${url_arm}"


## EXAMPLE: unpacking a flat package to find appropriate things
## Mount disk image on temp space
mountpoint=`yes | hdiutil attach -mountrandom /tmp -nobrowse app.dmg | awk '/private\/tmp/ { print $3 } '`
mountpoint_arm=`yes | hdiutil attach -mountrandom /tmp -nobrowse app_arm.dmg | awk '/private\/tmp/ { print $3 } '`


mkdir -p build-root/Applications
app_in_dmg=$(ls -d $mountpoint/*.app)
cp -R $app_in_dmg build-root/Applications

mkdir -p build-root-arm/Applications
app_in_arm_dmg=$(ls -d $mountpoint_arm/*.app)
cp -R "$app_in_arm_dmg" build-root-arm/Applications


# (cd build-root; pax -rz -f ../pkg/*/Payload)

# Obtain version info
#version=`curl -L http://www.laurenceanthony.net/software/antconc/ 2>/dev/null | grep "MacOS 10/11" | head -1 | awk -F'(' '{print $2}' | sed 's/(//g;s/)//g;s/<\/a><\/li>//g'| tr -d '\r'`
version=`/usr/libexec/PlistBuddy -c "Print :CFBundleShortVersionString" "$app_in_dmg"/Contents/Info.plist | awk '{ print $1}'`
version_arm=`/usr/libexec/PlistBuddy -c "Print :CFBundleShortVersionString" "$app_in_arm_dmg"/Contents/Info.plist | awk '{ print $1}'`


echo $version
echo $version_arm

hdiutil detach "${mountpoint}"
hdiutil detach "${mountpoint_arm}"


#creating a components plist to turn off relocation
/usr/bin/pkgbuild --analyze --root build-root/ Component-${munki_package_name}.plist
plutil -replace BundleIsRelocatable       -bool NO Component-${munki_package_name}.plist
plutil -replace BundleHasStrictIdentifier -bool NO Component-${munki_package_name}.plist

#build PKG
/usr/bin/pkgbuild --root build-root/ --identifier edu.umich.izzy.pkg.${munki_package_name} --install-location / --filter DISABLE_FILTERS_DISABLE_FILTERS --component-plist Component-${munki_package_name}.plist --version ${version} app.pkg

#clean up the component file
rm -rf Component-${munki_package_name}.plist

## ARM
/usr/bin/pkgbuild --analyze --root build-root-arm/ Component-${munki_package_name}arm.plist
plutil -replace BundleIsRelocatable       -bool NO Component-${munki_package_name}arm.plist
plutil -replace BundleHasStrictIdentifier -bool NO Component-${munki_package_name}arm.plist

#build PKG
/usr/bin/pkgbuild --root build-root-arm/ --identifier edu.umich.izzy.pkg.${munki_package_name} --install-location / --filter DISABLE_FILTERS_DISABLE_FILTERS --component-plist Component-${munki_package_name}arm.plist --version ${version_arm} app_arm.pkg

#clean up the component file
rm -rf Component-${munki_package_name}arm.plist

## Create pkg's
#/usr/bin/pkgbuild --root build-root/ --identifier edu.umich.izzy.pkg.$munki_package_name --install-location / --version $version app.pkg

## Find all the appropriate apps, etc, and then turn that into -f's
key_files=`find build-root -name '*.app' -or -name '*.plugin' -or -name '*.prefPane' -or -name '*component' -maxdepth 3 | sed 's/ /\\\\ /g; s/^/-f /' | paste -s -d ' ' -`

## Build pkginfo (this is done through an echo to expand key_files)
echo /usr/local/munki/makepkginfo -m go-w -g admin -o root app.pkg "${key_files}" --preinstall_script preinstall.sh | /bin/bash > app.plist

echo /usr/local/munki/makepkginfo -m go-w -g admin -o root app_arm.pkg "${key_files}" --preinstall_script preinstall.sh | /bin/bash > app_arm.plist

## Fixup and remove "build-root" from file paths
perl -p -i -e 's/build-root//' app.plist
perl -p -i -e 's/build-root//' app_arm.plist
## END EXAMPLE

# Build pkginfo
#/usr/local/munki/makepkginfo app.pkg > app.plist

plist=`pwd`/app.plist
plist_arm=`pwd`/app_arm.plist

# Obtain version info
#minver=`/usr/libexec/PlistBuddy -c "Print :installs:0:minosversion" "${plist}"`

# For silent packages, configure blocking_applications and unattended_install
# defaults write "${plist}" blocking_applications -array "VirtualBox.app" "/Path/To/Executable" "iTunesHelper"
# defaults write "${plist}" unattended_install -bool YES

# Change path and other details in the plist
defaults write "${plist}" installer_item_location "jenkins/${prefix}-${version}${suffix}.pkg"
defaults write "${plist}" minimum_os_version "10.15.0"
defaults write "${plist}" uninstallable -bool NO
defaults write "${plist}" name "${munki_package_name}"
defaults write "${plist}" display_name "${display_name}"
defaults write "${plist}" icon_name "${icon_name}"
#defaults write "${plist}" version "${version}"
defaults write "${plist}" supported_architectures -array x86_64
defaults write "${plist}" unattended_install -bool TRUE


# Change path and other details in the plist
defaults write "${plist_arm}" installer_item_location "jenkins/${prefix}-${version}-arm.pkg"
defaults write "${plist_arm}" minimum_os_version "10.15.0"
defaults write "${plist_arm}" uninstallable -bool NO
defaults write "${plist_arm}" name "${munki_package_name}"
defaults write "${plist_arm}" icon_name "${icon_name}"
#defaults write "${plist}" version "${version}"
defaults write "${plist_arm}" supported_architectures -array arm64
defaults write "${plist_arm}" unattended_install -bool TRUE


# Obtain display name from Izzy and add to plist
display_name=`/usr/local/bin/displaynametool "${munki_package_name}"`
defaults write "${plist}" display_name -string "${display_name}"
defaults write "${plist_arm}" display_name -string "${display_name}"

# Obtain description from Izzy and add to plist
description=`/usr/local/bin/descriptiontool "${munki_package_name}"`
defaults write "${plist}" description -string "${description}"
defaults write "${plist_arm}" description -string "${description}"

# Obtain category from Izzy and add to plist
category=`/usr/local/bin/cattool "${munki_package_name}"`
defaults write "${plist}" category "${category}"
defaults write "${plist_arm}" category "${category}"

# Make readable by humans
/usr/bin/plutil -convert xml1 "${plist}"
chmod a+r "${plist}"

# Make readable by humans for ARM
/usr/bin/plutil -convert xml1 "${plist_arm}"
chmod a+r "${plist_arm}"

# Change filenames to suit
mv app.pkg   ${prefix}-${version}${suffix}.pkg
mv app_arm.pkg   ${prefix}-${version}-arm.pkg
mv app.plist ${prefix}-${version}${suffix}.plist
mv app_arm.plist ${prefix}-${version}-arm.plist

# Notify trello

#ruby $HOME/jenkins-trello/trello-notify.rb "In Testing" "${display_name}" "${version}" "${prefix}-${version}.plist" $*

#ruby $HOME/jenkins-trello/trello-notify.rb "In Testing" "${munki_package_name}" "${version}" "${prefix}-${version}.plist" $*
#ruby $HOME/jenkins-trello/trello-notify.rb "In Testing" "${munki_package_name}arm" "${version_arm}" "${prefix}-${version_arm}-arm.plist" $*

$HOME/jenkins-trello/otto-notify "${munki_package_name}" "${version}" "${prefix}-${version}.plist" "${minver}" "${maxver}" "${key_files}" "${requires}" "${update_for}"  $*
$HOME/jenkins-trello/otto-notify "${munki_package_name}arm" "${version_arm}" "${prefix}-${version_arm}-arm.plist" "${minver}" "${maxver}" "${key_files}" "${requires}" "${update_for}"  $*
