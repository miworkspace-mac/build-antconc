#!/bin/bash

#NEWLOC=`curl -L http://www.laurenceanthony.net/software.html -A 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/536.26.17 (KHTML, like Gecko) Version/6.0.2 Safari/536.26.17' 2>/dev/null | /usr/local/bin/htmlq -a href a | grep .dmg | head -1`

#if [ "x${NEWLOC}" != "x" ]; then
#	echo "http://www.laurenceanthony.net"${NEWLOC}
#fi

VERS=`curl -L https://www.laurenceanthony.net/software/antconc/ | grep AntConc | grep Version | egrep -o "([0-9]{1,}\.)+[0-9]{1,}" | tr -d '.'`


if [ "x${VERS}" != "x" ]; then
	echo "http://www.laurenceanthony.net/software/antconc/releases/AntConc"${VERS}"/apple-intel/AntConc.dmg"
fi


